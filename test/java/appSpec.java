package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.awaitility.Awaitility.*;

@DisplayName("Usability unit tests")
public class appSpec {

    @Test
    public void withdraw_1001_not_allowed() {
        Balance balance = new Balance(1000);
        Amount amount = new Amount(1001);
        boolean res = Main.performTransfer(amount, balance);
        assertFalse(res,() -> "Withdraw 1001 from 1000 is not allowed");
    }

    @Test
    public void withdraw_two_500_zeros_balance() {
        Balance balance = new Balance(1000);
        Amount amount = new Amount(500);
        await().until(() -> Main.performTransfer(amount, balance) == true);
        await().until(() -> Main.performTransfer(amount, balance) == true);
        await().until(() -> balance.value.get() == 0);
    }
}
