package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.awaitility.Awaitility.await;
import static java.util.concurrent.TimeUnit.SECONDS;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {
    @Test
    public void withdraw_negative_amount_throw_exception() {
        Balance balance = new Balance(1000);
        Amount amount = new Amount(-1000);
        assertThrows(IllegalArgumentException.class, () -> {
            boolean res = Main.performTransfer(amount, balance);
        });
    }

    @Test
    public void withdraw_Int32Max_amount_throw_exception() {
        Balance balance = new Balance(1000);
        Amount amount = new Amount(Integer.MAX_VALUE);
        assertThrows(IllegalArgumentException.class, () -> {
            boolean res = Main.performTransfer(amount, balance);
        });
    }

    @Test
    public void should_prevent_toctou_overdraw() {
        Balance balance = new Balance(1000);
        Amount amount = new Amount(1000);
        assertTrue(Main.performTransfer(amount, balance));
        Main.performTransfer(amount, balance);
        await().atMost(2, SECONDS).until(() -> balance.value.get() == 0);
    }
}
