package app;

import java.lang.Thread;
import java.util.concurrent.CompletableFuture;

public class TransferService implements Runnable {

    private Amount amount;
    private Balance balance;
    // Result contains the balance after the transfer is complete
    public CompletableFuture<Integer> result;

    public TransferService(Amount amount, Balance balance) {
        this.amount = amount;
        this.balance = balance;
        this.result = new CompletableFuture<>();
    }

    public void run() {
        try {
            //The below line is a service delay simulator. Do not change.
            Thread.sleep(1000);
            while (true) {
                int currBalance = this.balance.value.get();
                int currAmount = this.amount.value;
                if (currBalance >= currAmount) {
                    if (this.balance.value.compareAndSet(currBalance, currBalance - currAmount)) {
                        this.result.complete(currBalance - currAmount);
                        break;
                    }
                } else {
                    this.result.completeExceptionally(new IllegalStateException("Not enough funds to complete transfer"));
                    break;
                }
                System.out.println("[i] Successfully transfered: " + this.amount.value);
                System.out.println("[i] Your new balance is: " + this.balance.value);
            }
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
}
