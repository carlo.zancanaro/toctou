package app;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String answer = "";
        Balance balance = new Balance(1000);
        Scanner scanner = new Scanner(System.in);

        System.out.println("[i] Your account balance is: " + balance.value);

        do {
            System.out.println("[?] Enter an amount to withdraw (e.g. 10, 1000): ");
            Amount amount = new Amount(Integer.parseInt(scanner.next()));

            if(!performTransfer(amount, balance)) {
                System.out.println("[e] The amount is higher than your account balance");
                return;
            }
            System.out.println("[?] Do you want to make another withdraw (y or n)?");
            answer = scanner.next();
        } while(answer.equals("y"));

    }

    /**
     * Checks if a given amount can be withdrawed
     * if so, it will call a service to make the withdraw
     * @return boolean
     */
    public static boolean performTransfer(Amount amount, Balance balance) {
        if (amount.value < 0 || amount.value == Integer.MAX_VALUE) {
            throw new IllegalArgumentException();
        }
        if (amount.value <= balance.value.get()) {
            System.out.println("[i] Going to make the withdraw");
            TransferService transferservice = new TransferService(amount, balance);
            Thread t = new Thread(transferservice);
            t.start();
            transferservice.result.whenComplete((bal, ex) -> {
                    if (ex == null) {
                        System.out.println("[e] an error occurred - did you have enough funds for the transfer?)");
                    } else {
                        System.out.println("[i] transfer complete!");
                    }
                });
            return true;
        }
        return false;
    }

}
