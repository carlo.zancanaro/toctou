package app;

import java.util.concurrent.atomic.AtomicInteger;

public class Balance {
    public AtomicInteger value;
    public Balance(int balance){
        this.value = new AtomicInteger(balance);
    }
}
